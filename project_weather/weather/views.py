from django.http import HttpResponse
from requests import get


def city(request):
    city_name = request.GET.get("city")
    key = "6bc8f74c58b38f1c4f045f8cee7c71c4"
    weather_api = f'https://api.openweathermap.org/data/2.5/weather?q={city_name}&APPID={key}'
    response = get(weather_api).json()

    if 'message' in response:
        return HttpResponse(f'<script>alert("City {city_name} does not exist!");</script>')
    else:
        return HttpResponse(f"<h2>Country: {response['sys']['country']}</h2>"
                            f"<h2>City: {city_name}</h2>"
                            f"<h2>Longitude: {response['coord']['lon']}</h2>"
                            f"<h2>Latitude: {response['coord']['lat']}</h2>"
                            f"<h2>Weather: {response['weather'][0]['main']}</h2>"
                            f"<h2>Temperature: {round((response['main']['temp'] - 273.15), 1)}</h2>")
